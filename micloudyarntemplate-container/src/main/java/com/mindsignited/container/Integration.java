package com.mindsignited.container;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.stereotype.Component;

/**
 * Created by nicholaspadilla on 2/21/15.
 */
@Component
public class Integration {
    @HystrixCommand(fallbackMethod = "recover")
    public String breakHystrix() {
        try {
            Thread.currentThread().sleep(2000);
            throw new IllegalStateException("dude - i broke!");
        } catch (InterruptedException ex) {

        }
        return "Should not reach this code...";
    }

    public String recover() {
        return "Hystrix Broke and called Recovery Method!";
    }
}