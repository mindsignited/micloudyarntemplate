package com.mindsignited.container;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableDiscoveryClient
@EnableCircuitBreaker
public class ContainerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContainerApplication.class, args);
    }

    @RestController
    public static class Controller {

        @Autowired
        private Integration integration;

        @RequestMapping("/greetings")
        public String greeting() {
            return "Right back at ya!";
        }

        @RequestMapping("/break")
        public String breakHystrix() {
            String ret = integration.breakHystrix();
            return ret;
        }

    }

}
