miclouyarntempalte
===================

MI Cloud Yarn Template Server using Spring and Hadoop with Yarn.

* Please setup Hadoop and Yarn using this link, for development.   
   `https://github.com/spring-projects/spring-xd/wiki/Hadoop-Installation`

Add this to the `${HADOOP_HOME}/etc/hadoop/yarn-site.xml` for development only.

    <property>
        <name>yarn.log-aggregation-enable</name>
        <value>true</value>
    </property>
    <property>
         <name>yarn.scheduler.minimum-allocation-mb</name>
         <value>256</value>
    </property>
    <property>
        <name>yarn.nodemanager.delete.debug-delay-sec</name>
        <value>3600</value>
    </property>

Update this file `${HADOOP_HOME}/etc/hadoop/capacity-scheduler.xml` for development, as you will only be able to use 10% of
your system resources to run the cluster.  Increase the value by one, as needed.  If an application sits in an ACCEPTED state
for too long, your probably at the limit of what you can reserve.

    <property>
    <name>yarn.scheduler.capacity.maximum-am-resource-percent</name>
    <value>0.1</value>
    <description>
      Maximum percent of resources in the cluster which can be used to run
      application masters i.e. controls number of concurrent running
      applications.
    </description>
    </property>



* You must have Gradle, using 2.2.1 right now, installed locally.

* You'll need to have keycloak 1.1.0.Final running locally, also import the micloud-realm.json file. For Security.

* To build a distribution you just run `./gradlew clean build` - the artifacts will be in `${projectName}-dist/`.

* You can then run `./serverBoot.sh` to get it running on yarn locally.

* If it works you can run `yarn application -list` to see if `miclouyarntempalte` is in the RUNNING state, give it several minutes
in RUNNING state to ensure it started successfully - or monitor the logs.



* NOTE: If you have YARN/Hadoop failures - that talk about not finding /bin/java - do this.

   `sudo ln -s <path-to-your-java> /bin/java`

- Need to figure out how to best debug these sorts of applications.

---

Using this project as a base for a new one
==========================================

FIXME: need to streamline this more.  I am sure we can make this easier.

Prerequirement.
---------------
You must first have configured the `~/.m2/settings.xml` to have this basic setup

    <settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
        <servers>
            <server>
                <id>mindsignited-repo</id>
                <username>Your Bitbucket Username</username>
                <password>You Bitbucket Password</password>
            </server>
            <server>
                <id>mindsignited-repo-snapshot</id>
                <username>Your Bitbucket Username</username>
                <password>You Bitbucket Password</password>
            </server>
        </servers>
    </settings>

1. Copy this projects contents, minus the `.git/` directory. <br/> <br/>
2. We'll need to update the projectName and files/folders that need that.
    1. These are the files we need to update.
        1. `gradle.properties` <br/> <br/>
            `projectName = <projectName>` <br/> <br/>
        2. `start.sh` <br/> <br/>
            `CONFIG_APPLICATION=<projectName>` <br/> <br/>
        3. `serverBoot.sh` <br/> <br/>
            `CONFIG_APPLICATION=<projectName>` <br/> <br/>
        4. `micloudyarntemplate-appmaster/src/main/resources/application.yml` <br/> <br/>
            `spring.application.name: <projectName>` <br/> <br/>
        5. `micloudyarntemplate-appmaster/micloudyarntemplate-appmaster.iml` needs to become <br/> <br/>
            `micloudyarntemplate-appmaster/<projectName>-appmaster.iml` <br/> <br/>
        6. `micloudyarntemplate-client/src/main/resources/application.yml` <br/> <br/>
            `spring.application.name: <projectName>` <br/> <br/>
        7. `micloudyarntemplate-client/micloudyarntemplate-client.iml` needs to become  <br/> <br/>
            `micloudyarntemplate-client/<projectName>-client.iml` <br/> <br/>
        8. `micloudyarntemplate-container/src/main/resources/bootstrap.yml` <br/> <br/>
            `spring.application.name: <projectName>` <br/> <br/>
        9. `micloudyarntemplate-container/micloudyarntemplate-container.iml` needs to become <br/> <br/>
            `micloudyarntemplate-container/<projectName>-container.iml` <br/> <br/>
        10. `micloudyarntemplate.iml` needs to become <br/> <br/>
            `<projectName>.iml` <br/>
    2.  These are the folders that need a name change.
        1. `micloudyarntemplate-appmaster/` needs to become  <br/> <br/>
            `<projectName>-appmaster/` <br/> <br/>
        2. `micloudyarntemplate-client/` needs to become <br/> <br/>
            `<projectName>-client/` <br/> <br/>
        3. `micloudyarntemplate-container/` needs to become <br/> <br/>
            `<projectName>-container/` <br/> <br/>
        4. `micloudyarntemplate-dist/` needs to become <br/> <br/>
            `<projectName>-dist/`
3. Import the project into IntelliJ IDEA using the `build.gradle` file, as a Gradle Project. <br/> <br/>
4. Open the Gradle view in IntelliJ and Refresh the new project. <br/> <br/>
5. You can now build the project -- `./gradlew clean build`.
6. You can run it as a SpringBoot or as a Yarn application.  Yarn - `./serverBoot.sh`   Boot - `./serverBoot.sh fatJar`
7. You can reach the application after booting with this url, assuming no changes to default ports.
    Through Zuul --- `http://127.0.0.1:8765/<projectName>/greetings` -- You may need to add the Application to IAM and to your User Roles.
    Through Boot --- `http://127.0.0.1:8089/greetings` - might have issues with authentication as it requires JWT bearer auth.
8. Your up and running! You can start making changes as needed.

---
NOTES:

1. You can update the `*-container` application as it is what is run.
2. Update needed dependencies in `build.gradle` for the gradle project `project("${projectName}-container")` on line 143.
