#!/bin/bash

## MI Yarn Template Server

CONFIG_APPLICATION=micloudyarntemplate

if [ "$1" == "debug" ]; then
   ./gradlew -PjvmArguments="-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5011" ${CONFIG_APPLICATION}-client:clean ${CONFIG_APPLICATION}-client:bootRun --debug-jvm
else
   ./gradlew ${CONFIG_APPLICATION}-client:clean ${CONFIG_APPLICATION}-client:bootRun
fi
